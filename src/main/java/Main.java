import java.io.File;

public class Main {

    public static void main(String... args) throws Exception {
        if (args.length < 1) {
            System.out.println("Please, enter a path to configuration file");
        } else {
            ServerRunner serverRunner = new ServerRunner(new File(args[0]));
            serverRunner.run();
        }
    }

}