package utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;

public final class GZipUtils {

    private GZipUtils() {
    }

    /**
     * Этот метод делает декомпрессию сжатых данных
     *
     * @param compressed массив байтов входных данных
     * @param charset    кодировка
     * @return декомпрессированная строка
     */
    public static String unzip(final byte[] compressed, final Charset charset) {
        if ((compressed == null) || (compressed.length == 0)) {
            throw new IllegalArgumentException("Cannot unzip null or empty bytes");
        }
        if (!isZipped(compressed)) {
            return new String(compressed);
        }

        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(compressed)) {
            try (GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream)) {
                try (InputStreamReader inputStreamReader = new InputStreamReader(gzipInputStream, charset)) {
                    try (BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
                        StringBuilder output = new StringBuilder();
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            output.append(line);
                        }
                        return output.toString();
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to unzip content", e);
        }
    }

    /**
     * Проверяет записано ли в заголовке файла магическое число
     * символизирующее, что данный "файл" был подвергнут компрессии
     *
     * @param compressed входной файл
     * @return сжат или нет
     */
    public static boolean isZipped(final byte[] compressed) {
        return (
                compressed[0] == (byte) (GZIPInputStream.GZIP_MAGIC))
                && (compressed[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8)
        );
    }

}