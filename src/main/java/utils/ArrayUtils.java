package utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Методы для преобразования массивов в списке
 * и наоборот
 */
public final class ArrayUtils {

    public static synchronized byte[] toArray(final List<Byte> list) {
        final byte[] array = new byte[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static synchronized List<Byte> toList(final byte[] array) {
        final List<Byte> list = new ArrayList<>(array.length);
        for (byte b : array) {
            list.add(b);
        }
        return list;
    }

}
