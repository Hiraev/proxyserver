import cache.CacheManager;
import cache.Packet;
import log.Logger;
import org.eclipse.jetty.client.api.Response;
import org.eclipse.jetty.proxy.AsyncProxyServlet;
import org.eclipse.jetty.util.Callback;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static constants.Constants.*;

public class AsyncProxyServer extends AsyncProxyServlet {

    private final Map<Integer, Packet> packets;
    private CacheManager cacheManager;
    private Logger logger;

    public AsyncProxyServer(CacheManager cacheManager, Logger logger) {
        this.packets = new ConcurrentHashMap<>();
        this.cacheManager = cacheManager;
        this.logger = logger;
    }

    @Override
    protected synchronized void onProxyResponseSuccess(
            HttpServletRequest clientRequest,
            HttpServletResponse proxyResponse,
            Response serverResponse
    ) {

        //Ключ для сохранения в кеше (сохраняем по адресу, на который было обращение)
        final String key = clientRequest.getRequestURL().toString()
                + ((clientRequest.getQueryString() != null) ? clientRequest.getQueryString() : "");
        final Packet packet = packets.get(getRequestId(clientRequest));
        if (packet != null) {
            cacheManager.put(key, proxyResponse, packet.getData());
            packets.remove(getRequestId(clientRequest));
        }
        logger.log(clientRequest.getMethod() + " <<< ", clientRequest, proxyResponse);
        super.onProxyResponseSuccess(clientRequest, proxyResponse, serverResponse);
    }

    @Override
    protected void onResponseContent(
            HttpServletRequest request,
            HttpServletResponse response,
            Response proxyResponse,
            byte[] buffer,
            int offset,
            int length,
            Callback callback
    ) {
        super.onResponseContent(request, response, proxyResponse, buffer, offset, length, callback);

        if (isCachable(response)) {
            synchronized (packets) {
                // Кладем частичку данных в map пакетов, идентифицируя данный пакет с помощью ip клиента
                // Ключ для идентификации откуда пришел запрос и кому предназначен ответ
                int requestId = getRequestId(request);
                if (packets.containsKey(requestId)) {
                    packets.get(requestId).putPacket(buffer);
                } else {
                    final Packet packet = new Packet();
                    packet.putPacket(buffer);
                    packets.put(requestId, packet);
                }
            }
        }
    }

    /**
     * Переопределяем метод service, которые принимает запросы от клиентов
     * и обрабатывает их. Если на данный запрос можно отдать что-то из кэша,
     * то отдаем, если нет, то предоставлем родительскому классу обработку запроса
     *
     * @param req запрос
     * @param res ответ, который нужноазполнить
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void service(
            ServletRequest req,
            ServletResponse res
    ) throws ServletException, IOException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        HttpServletResponse httpServletResponse = (HttpServletResponse) res;

        final String key = httpServletRequest.getRequestURL().toString()
                + ((httpServletRequest.getQueryString() != null) ? httpServletRequest.getQueryString() : "");

        /**
         * Пытаемся инициализировать response с помощъю кэш-менеджера
         * Если не получится, он вернет false и тогда инициазацию ответа
         * делегируем родительскому классу вызывая super.service()
         */
        if (!cacheManager.initResponse(key, httpServletResponse)) {
            super.service(req, res);
        }
        logger.log(httpServletRequest.getMethod() + " >>> ", httpServletRequest, httpServletResponse);
    }

    /**
     * Здесь мы проверяем заголовки ответа сервера
     * Если в ответе в поле Cache-Control или в поле
     * Pragma присутсвует no-cache, значит нельзя кэшировать данные
     *
     * @param response ответ от сервера
     * @return можно кэшировать или нет
     */
    private synchronized boolean isCachable(final HttpServletResponse response) {
        final String cacheControl = response.getHeader(CACHE_CONTROL_HEADER);
        final String pragmaHeader = response.getHeader(PRAGMA_HEADER);
        return (
                cacheControl == null || !cacheControl.contains(NO_CACHE_MARK))
                && (pragmaHeader == null || !pragmaHeader.contains(NO_CACHE_MARK)
        );
    }

}