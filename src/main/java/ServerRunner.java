import cache.CacheManager;
import log.Logger;
import log.STDOutLogger;
import org.eclipse.jetty.proxy.ConnectHandler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public final class ServerRunner {

    private int port;
    private int lifetime;
    private long cacheSize;
    private File file;

    public ServerRunner(final File file) {
        this.file = file;
    }

    public void run() throws Exception {
        loadProperties();
        Server server = new Server(port);
        server.setHandler(initAndGetHandlers());
        server.start();
        server.join();
    }

    private void loadProperties() {
        try {
            Properties properties = new Properties();
            properties.load(new FileReader(file));
            port = Integer.valueOf(properties.getProperty("port"));
            lifetime = Integer.valueOf(properties.getProperty("lifetime"));
            cacheSize = Integer.valueOf(properties.getProperty("cache_size"));
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
            throw new RuntimeException("Problems with properties");
        }
    }

    private HandlerCollection initAndGetHandlers() {
        /**
         * Инициализируем кэш-менеджер, задаем его максимальный размер и время жизни данных
         */
        final CacheManager cacheManager = new CacheManager(cacheSize, lifetime);
        final Logger logger = new STDOutLogger();

        cacheManager.registerLogger(logger);
        final AsyncProxyServer asyncProxyServer = new AsyncProxyServer(cacheManager, logger);
        final ServletHolder proxyServletHolder = new ServletHolder(asyncProxyServer);
        proxyServletHolder.setInitParameter("maxThreads", "4");

        final HandlerCollection handlers = new HandlerCollection();

        final ServletHandler servletHandler = new ServletHandler();
        servletHandler.addServletWithMapping(proxyServletHolder, "/*");

        final ConnectHandler proxy = new ConnectHandler();

        handlers.addHandler(servletHandler);
        handlers.addHandler(proxy);
        handlers.addHandler(servletHandler);
        return handlers;
    }
}
