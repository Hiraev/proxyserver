package log;

import org.joda.time.Instant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Класс реализующий интерфейс Logger
 * Логгирует в стандартный выход (консоль, терминал)
 */
public class STDOutLogger implements Logger {

    @Override
    public synchronized void log(String string, HttpServletRequest request, HttpServletResponse response) {
        System.out.println(new Instant() + " " + request.getRemoteHost() + ": " + string + " " + request.getRequestURL() + " " /**+ request.getQueryString()*/);
    }

    @Override
    public synchronized void log(String string) {
        System.out.println(new Instant() + ": " + string);
    }

}
