package log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Logger {

    void log(String string, HttpServletRequest request, HttpServletResponse response);
    void log(String string);

}
