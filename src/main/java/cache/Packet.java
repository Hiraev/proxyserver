package cache;

import java.util.ArrayList;
import java.util.List;

import static utils.ArrayUtils.toList;

/**
 * Пакет данных
 * Позволяет записывать в себя данные порциями
 */
public class Packet {

    private List<Byte> packets;

    public Packet() {
        packets = new ArrayList<>();
    }

    public synchronized void putPacket(byte[] packet) {
        packets.addAll(toList(packet));
    }

    public synchronized List<Byte> getData() {
        return packets;
    }

}