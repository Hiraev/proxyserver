package cache;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

import static constants.Constants.CONTENT_ENCODING_HEADER;
import static constants.Constants.CONTENT_LENGTH_HEADER;
import static utils.ArrayUtils.toArray;
import static utils.GZipUtils.isZipped;
import static utils.GZipUtils.unzip;

/**
 * Класс, в котором мы храним информацию о кешированном ответе
 * data - данные;
 */
final class CachedResponse {

    private int status;
    private byte[] data;
    private Locale locale;
    private String contentType;
    private String charsetEncoding;
    private long createdTime;
    private Map<String, String> headers = new HashMap<>();

    CachedResponse(final HttpServletResponse response, final List<Byte> data) {
        final Collection<String> headerNames = response.getHeaderNames();

        for (String headerName : headerNames) {
            headers.put(headerName, response.getHeader(headerName));
        }

        this.data = toArray(data);
        this.locale = response.getLocale();
        this.status = response.getStatus();
        this.createdTime = System.currentTimeMillis();
        this.contentType = response.getContentType();
        this.charsetEncoding = response.getCharacterEncoding();
    }

    /**
     * Данный метод инициализирует полученный объект ответа значениями, которое
     * хранятся в полях: заголовки ответа(headers) и тело(data)
     *
     * @param response ответ, в который нужно записать все данные
     */
    void initResponse(final HttpServletResponse response) {
        response.setLocale(locale);
        response.setStatus(status);
        response.setContentType(contentType);
        response.setContentLength(data.length);
        response.setCharacterEncoding(charsetEncoding);

        headers.forEach(response::addHeader);

        final Charset charset = Charset.forName(charsetEncoding.toUpperCase());

        try {
            /**
             * Если данные были сжаты, то делаем декомпрессию и поправляем
             * заголовки ответа, после декомпресии меняется размер данных,
             * поэтому его тоже меняем
             */
            if (isZipped(data)) {
                final byte[] unzipped = unzip(data, charset).getBytes();
                response.setHeader(CONTENT_ENCODING_HEADER, "identity");
                response.setHeader(CONTENT_LENGTH_HEADER, String.valueOf(unzipped.length));
                response.setContentLength(unzipped.length);
                response.getWriter().write(new String(unzipped, charset));
            } else {
                /** Если данные не были сжаты, то просто записываем данные, которые у нас уже есть */
                response.getWriter().write(new String(data, charset));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    long getCreatedTime() {
        return createdTime;
    }

    int getSize() {
        return data.length;
    }

}
