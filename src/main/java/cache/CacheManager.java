package cache;

import log.Logger;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Класс для управления кэшем
 * Записывает в кэш, достает оттуда
 * Следит за тем, чтобы кэш не переполнялся
 * Удаляет из кэша данные, которые находятся там слишком долго
 */
public class CacheManager {

    private long maxSize;
    private long lifetime;
    private Logger logger;
    private final Cache cache;
    private TimerTask cleaner;
    private static String loggerPrefix = "CACHE ";

    /**
     * @param maxSize  максимальный размер кэша в байтах
     * @param lifetime максимальная длительность жизни кешированного файла в секундах
     */
    public CacheManager(final long maxSize, final long lifetime) {
        this.cache = new Cache();
        long minLifeTime = 5;
        if (lifetime < minLifeTime) {
            this.lifetime = minLifeTime * 1000;
        } else {
            this.lifetime = lifetime * 1000;
        }
        if (maxSize > 0) {
            this.maxSize = maxSize;
            initCleaner();
            final Timer timer = new Timer();
            /** Запускаем чистку кэша по расписанию через каждые lifetime миллисекунд */
            timer.schedule(cleaner, 0, lifetime);
        }
    }

    public void registerLogger(final Logger logger) {
        this.logger = logger;
    }

    /**
     * Инициализирует чистильшик кэша
     * Через каждые lifetime миллисекунд удаляет из кэша
     * данные, которые живут уже больше lifetime
     */
    private void initCleaner() {
        cleaner = new TimerTask() {
            @Override
            public void run() {
                /** Выполняем синхронизацию по кэшу, чтобы он не менялся пока идет чистка*/
                synchronized (cache) {
                    try {
                        while (cache.isNotEmpty() && System.currentTimeMillis() - cache.oldestCreatedTime() > lifetime) {
                            final String removedUrl = cache.removeOldest();
                            if (logger != null)
                                logger.log(
                                        loggerPrefix +
                                                "removed (OUTDATED): " +
                                                cache.getSize() +
                                                ": " +
                                                removedUrl
                                );
                        }
                    } catch (IllegalStateException e) {
                        if (logger != null) logger.log("CATCH EXCEPTION " + e.getMessage());
                    }
                }
            }
        };
    }

    public void put(final String url, final HttpServletResponse response, final List<Byte> buffer) {
        synchronized (cache) {
            if (maxSize < buffer.size()) {
                if (logger != null) logger.log(loggerPrefix + "too big buffer: " + buffer.size() + " bytes");
                return;
            }
            while (cache.getSize() + buffer.size() > maxSize) {
                final String removedUrl = cache.removeOldest();
                if (logger != null)
                    logger.log(loggerPrefix + "removed (NO SPACE) " + cache.getSize() + ": " + removedUrl);
            }
            cache.put(url, response, buffer);
            if (logger != null) logger.log(loggerPrefix + "inserted: " + url);
        }
    }

    public boolean initResponse(final String url, final HttpServletResponse response) {
        synchronized (cache) {
            if (!containsKey(url)) return false;
            logger.log(loggerPrefix + "RETURN " + url);
            cache.get(url).initResponse(response);
            return true;
        }
    }

    public synchronized boolean containsKey(final String url) {
        return cache.contains(url);
    }

}
