package cache;

import javax.servlet.http.HttpServletResponse;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Кэш
 * Хранит данные в структуре Map типа ключ-знаение
 * Ключ - это url + query, а значени - соответсвующие данные
 * <p>
 * Так же здесь для удобства присутсвует orderedKeys
 * в котором располагяются ключи в порядке добавления
 * Это позволяет быстро находить самые старые ключи и удалять
 * соответвующие им элементы
 */

final class Cache {

    private Map<String, CachedResponse> cache = new ConcurrentHashMap<>();
    private Deque<String> orderedKeys = new LinkedBlockingDeque<>();
    private int size;

    synchronized void put(final String url, final HttpServletResponse response, final List<Byte> buffer) {
        orderedKeys.add(url);
        final CachedResponse cachedResponse = new CachedResponse(response, buffer);
        cache.put(url, cachedResponse);
        size += cachedResponse.getSize();
    }

    synchronized boolean contains(final String url) {
        return cache.containsKey(url);
    }

    synchronized CachedResponse get(String url) {
        if (cache.containsKey(url)) {
            return cache.get(url);
        }
        orderedKeys.remove(url); //На всякий случай, если oldest
        throw new IllegalStateException("There is no such key in the cache");
    }

    private synchronized void remove(final String url) {
        if (cache.containsKey(url)) {
            size -= cache.get(url).getSize();
            cache.remove(url);
        }
    }

    int getSize() {
        return size;
    }

    /**
     * Удалить самый первый элемент, то есть тот
     * который дольше всех находится в кэше
     *
     * @return ключ - соответсвующий удаленному элементу
     */
    synchronized String removeOldest() {
        final String first = orderedKeys.pollFirst();
        remove(first);
        return first;
    }

    /**
     * Время, когда был добавлен самый старые элемент в
     * кэше
     *
     * @return время в миллисекундах
     */
    synchronized long oldestCreatedTime() {
        return get(orderedKeys.getFirst()).getCreatedTime();
    }

    synchronized boolean isNotEmpty() {
        return !cache.isEmpty();
    }

}
